[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

> Parking area(s) availability prediction

By means of the information retrieved from the different parking related context entities (i.e. **OnStreetParking** and **OffStreetParking**), this service:

1. Returns the current status of a particular parking area (or presents the overall information of all the registered zones)
2. Predicts the probability of finding a free parking lot within a particular area of the city (i.e. in the next 15, 30, 45 and 60 minutes)

**NOTE:** It is strongly recommended that the reader has previous background on **FIWARE Generic Enablers** (i.e. Orion Context Broker, STH Comet, etc.). 

# Service architecture

We have followed the micro-service paradigm to split the different functionalities into different modules. The figure below outlines how we interact with the Synchronicity framework to extract information from (i.e. bottom part) the different IoT infrastructures (i.e. Smart Cities). On top of this, we can observe components/micro-services, which are detailed below (as a matter of fact, technically speaking, this figure represents the Docker images that shape the atomic service).

| ![Estimator service architecture](docs/parking-estimator-v2.png) |
|:--:|
| Parking estimator architecture |

- **IoT Data Manager**  [[Docker Hub](https://hub.docker.com/r/synchronicityiot/estimator-data-manager)]: Module responsible for extracting data from the underlying external IoT infrastructures (Orion CBs and historical APIs) and forwarding the information to the Data Storage Cluster.
- **Data storage cluster** (based on *Elasticsearch*) [[Docker Hub](https://hub.docker.com/_/elasticsearch)]: High-performance database + search engine, where entities, historic data, models and predictions will be stored (and queried). We use the keyword "cluster" because we can easily replicate the funcionality of a single node (current configuration) into various, which will replicate the information stored.
- **Training engine** [[Docker Hub](https://hub.docker.com/r/synchronicityiot/trainer-estimation-generic)]: AI-based engine responsible for training and generating new predictions models, based upon the information previously collected by the *IoT Data Manager* and saved on the *Data storage cluster*. 
- **Inference engine** [[Docker Hub](https://hub.docker.com/r/synchronicityiot/estimation-generic)]: AI-based engine that takes the models generated by the trainer and produces the predictions/estimations).
- **Smart Parking (Service-Manager)** [[Docker Hub](https://hub.docker.com/r/synchronicityiot/estimator-data-manager)]: Service layer that goes on top of the Data Storage Cluster and generates the output of the prediction engines in NGSIv2 format. In the end, this is the point where users will request and gather the estimation results from the service.
- **Data Management** (based on Kibana) [[Docker Hub](https://hub.docker.com/_/kibana)]: Dashboard where we can visualize and monitor any kind of data stored in *Elasticsearch* (i.e. *Data storage clusted*)
- **Orion CB (internal)** [[Docker Hub](https://hub.docker.com/r/synchronicityiot/orion)]: FIWARE's General Enabler (GE) instance that will be used in the context (internally) of this atomic service to keep track (concentrate) of the various context elements. Moreover, the output coming from the AI prediction engine is saved as an additional attribute within the context element information, behaving like a sort of caching mechanism. Besides, we cannot forget that Orion CB works together with a [**MongoDB**](https://hub.docker.com/_/mongo) instance. As a matter of fact, having this component in the architecture opens the door to the connection to connecting the atomic service with other FIWARE Generic Enablers (e.g. Quantum Leap, Grafana, etc.).

*NOTE*: The instance of Orion as part of the atomic service is about to be deprecated, due to the lack of real possibilities/interest to connect to another FIWARE GEs at this level.

# Interfaces and APIs

The upper part of the figure reflects the REST API that is open to users. Namely, the *Smart Parking* component behaves as a RESTful API that presents users the results from the AI engine(s). As a summary, these are the (three) methods that are currently available:

| Method | Description | Params (URL) | Output |
|:---------------------------------------------|:---------------------------------------------|:----------------------------------------------|-------------------------------------------------------------------------------------------|
| ```/parkingareas``` | Information (NGSIv2) of all the parking areas | [Optional] page <br> [Optional] items_per_page <br> [Optional] type |  Array of OnStreetParking/OffStreetParking context elements |
| ```/parkingareas/<id>``` | Information of **a particular** parking area (`<id>`) | id | Single OnStreetParking/OffStreetParking context element |
| ```/parkingareas/<id>/freeSpotEstimation``` | Parking availability prediction for the next hour | id | Single OnStreetParking/OffStreetParking context element (including the prediction values) |

 Anyway, the reader can find a more complete documentation (based on Apiary) of the API [here](https://synchronicityparkingestimator.docs.apiary.io/#).

# Repository structure

Before showing how to configure & deploy the atomic service, let's see how the repo is structured and, more importantly, what we have to touch:

- `docker-compose.yml` (file): This can be seen as the launcher, from where all the components will be subsequently deployed. Mainly, we can edit few things here (e.g. endpoints and volumes).
- `.env`(file): Placeholder for common/global environment variables. For example, the versions of the containers that are going to be deployed.
- `config` (folder): Placeholder for all the configuration files.
- `docs` (folder): material for documentation (e.g. this README file), like images, etc. **NOTE: No need to touch anything here**.
- `logs` (folder): Sink where every component will dump their logging files. **NOTE: No need to touch anything here**.

- deployment: In this folder we have included all the configuration files that are necessary to successfully deploy the atomic service (via Docker-compose). **NOTE** It is deemed necessary to manually include these configuration files (one per module or microservice), as will be explained in the next section.

# Configuration

> For the sake of not sharing private information (e.g. endpoints, passwords, etc.) we do not share actual configuration files (e.g. `config.js`). Instead, we basically copy the same content onto a sample file (e.g. `config.js.sample`) and comment and explain every field that is tweakable. To carry out a real deployment of the component, users must rename the sample files (basically, erase the `.sample` suffix) and complete the real configuration.

In this section we outline the main modifications that are to be done prior to run the atomic service. Basically, we introduce all the configurable elements and link to a more thorough documentation. **NOTE: It is worth highlighting that every sample files contains a thorough explanation of each of the fields and attributes that shape them**.

## docker-compose.yml configuration

This legacy file, by default, will deploy a single instance of all the following components on the machine that runs the execution command (see below). Technically speaking, users only have to configure a single environment variable, `SUBSCRIPTIONS_ENDPOINT`, saying where underlying ORION's CBs have to send their notifications. For example (taking into account that the default listening port is 6000), the variable would look like this: `http://my-url-or-ip-address:6000`.

```bash
environment:
    SUBSCRIPTIONS_ENDPOINT: <endpoint>
```

**NOTE:** As we are using a local deployment (i.e. all components are going to be deployed on the same machine), endpoints will basically correspond to other services' hostnames. For example, services will directly address to `elasticsearch`. Hence, 

## IoT Data Manager (`config/data-manager`)

> More detailed information can be found in the sample files ([auth.js.sample](https://gitlab.com/synchronicity-iot/parking-estimator/blob/master/config/data-manager/auth.js.sample) and [config.js.sample](https://gitlab.com/synchronicity-iot/parking-estimator/blob/master/config/data-manager/config.js.sample)) and on [Docker Hub](https://hub.docker.com/r/synchronicityiot/estimator-data-manager)

For this module, we have two different configuration files: *1-* `auth.js`, where all the authentications means take place (e.g. OAuth 2.0 tokens); *2-* `config.js`, which contains service-oriented specific information (e.g. listening port, data source - underlying Orion CBs location and conext entities, historical dump enabled/disabled, etc.).

## Smart Parking/Service Manager (`config/service-manager`)

> More detailed information can be found in the sample file ([config.js.sample](https://gitlab.com/synchronicity-iot/parking-estimator/blob/master/config/service-manager/config.js.sample)) and on [Docker Hub](https://hub.docker.com/r/synchronicityiot/estimator-parking-service-manager).

In this case, unlike the IoT Data Manager, we only need a configuration file (`config.js`), where we have to define: listening port, output format (e.g. NGSIv2), Elasticsearch endpoint, etc.

## Training engine (`config/trainers`)

> More detailed information can be found in the sample file ([config.ini.sample](https://gitlab.com/synchronicity-iot/parking-estimator/blob/master/config/trainers/config.ini.sample)) and on [Docker Hub](https://hub.docker.com/r/synchronicityiot/trainer-estimation-generic).

The configuration file here (`config.ini`) defines the setup that the AI framework (i.e. [Keras](https://keras.io/) and/or [Scikit Learn](https://scikit-learn.org/stable/#)). Summing up, chosen training algorithms, Elastic indices (and endpoints) and internal scheduler(s) are configured here.

## Inference engine (`config/inferencers`)

> More detailed information can be found in the sample file ([config.ini.sample](https://gitlab.com/synchronicity-iot/parking-estimator/blob/master/config/inferencers/config.ini.sample)) and on [Docker Hub](https://hub.docker.com/r/synchronicityiot/estimation-generic).

On top of the models generated by the different trainers, inference engines define their behavior via a namesake configuration file called `config.ini` (no surprises here). **Apart from the internal scheduler that awakes the module, both trainer and inferencer must have the very same configuration (i.e. Elasticsearch indices, ML algorithms, time windows, etc.)**.

## Elasticsearch (`config/elasticsearch/elasticsearch.yml`)

For this atomic service we have set a single-node configuration, where, as its name hints, a unique node will save all the information. For more complex setups, please refer to the [Elasticsearch documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html).

That is, **users do not need to tamper anything to run this service**.

## Kibana (`config/kibana/kibana.yml`))

In this case, Kibana only needs to point at Elasticsearch. That is, **users do not need to tamper anything to run this service**.

Anyway, should the reader wants a deeper glance at this component, please refer to the official [Kibana documentation](https://www.elastic.co/guide/en/kibana/current/index.html).

# System requirements

It is important to highlight that this atomic service makes an intensive use of Keras and (subjacently) Tensor Flow, which are quite demanding in terms of system requirements. [Here](https://www.nvidia.com/en-gb/data-center/gpu-accelerated-applications/tensorflow/) we have found some figures on the minimum configuration to make the system run:

- Ubuntu (>=) 14.04
- CPU Architecture (x86_64)
- System memory (RAM) - 16GB (32GB recommended)
  - Elastic Search (more info [here](https://www.elastic.co/guide/en/elasticsearch/guide/current/hardware.html)), though recommending 64GB of RAM, they state that 32GB or 16GB machines are also common.
- GPU (performance can be boosted with the presence of GPU units)
- Docker installed
- Docker-compose installed
- TensorFlow-compatible microprocessors (see note below). **NOTE:** We have witnessed some issues when it comes to run TensorFlow on "oldish" machines. It may happen that old microprocessors are not compatible with some instructions sets used by this framework, leading to a container emergency stop.

# Service deployment (via Docker compose)

**NOTE:** It goes without saying that both [Docker](https://docs.docker.com/install/) and [Docker-compose]((https://docs.docker.com/compose/install/)) must be installed on the system

## Step 0 - Increase the virtual memory size (at the host)

In order to anticipate to potential issues when deploying this container, the maintainers recommend that we run the following command (which increases the maximum VM memory size) beforehand. More information on this can be found [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html)

```
sysctl -w vm.max_map_count=262144
```

## Step 1 - Configuration files

Follow what is explained [here](#configuration) and nail down the setup that suit the user's IoT infrastructure, from Data sources to the output format.

### Step 2 - Run the docker-compose

Just run the command:

```
docker-compose up
```

# Service Operation (Optional reading)

In this section, we will describe, in few lines, what this service actually does behind the scenes. As a matter of fact, everything is automatically done, so users do only have to worry about how to call and get the estimations:

1. (*IoT Data Manager*) Discovery of all **OnStreetParking** and **OffStreetParking** context elements, whose context element descriptions are saved on the Data storage cluster. 
2. (*IoT Data Manager*) From the list of all available devices, we harvest all the historical information that the underlying IoT infrastructures have saved. To do this, these external source must be compliant to the Historical Data API defined in the scope of Synchronicity (actually, very similar to [STH-Comet](https://fiware-sth-comet.readthedocs.io/en/latest/)). **Note:** Users can disable this function by setting to `false` the variable `historical_data_enabled` of each data source in the file ```config/data-manager/config.js```.
3. (*IoT Data Manager*) Whereas the historical data dump (*step 2*) gathers all the past information, we also get subscribed to IoT infrastructures' ORION CBs, in order to hook at forthcoming events. This way we do not have to worry about periodically polling whether the platforms have new data/observations. **Note:** Users can disable this function by setting to `false` the variable `subscriptions_enabled` of each data source in the file ```config/data-manager/config.js```.
4. (*Training engine*) In parallel to the data ingestion carried out by the Data Manager, the Training module will periodically re-train the models, storing the output in Elasticsearch. **NOTE:** We can nail down the scheduling options of this re-training process in ```config/trainers/config.ini``` file.
5. (*Inference engine*) The models generated in *step 3* will be the basis of the predictions, carried out here. In a second scheduled process, the inference engine will be generating new predictions (e.g. every 15 minutes), and will save the output in Elasticsearch. **NOTE:** We can nail down the scheduling options of this inferencing process in ```config/inferencers/config.ini``` file.
6. (**Smart Parking*) As a last step, users can freely query this service layer and retrieve the actual value of the estimation.

**NOTE:** As a matter of fact, every context element will have its own model, meaning that the API output will be tailored to the specific behavior of each parking area.

# Screenshots

For the sake of illustration, we include here a couple of screenshots that show how the Data Management layer can look like (NOTE: we provide a sample layout in ```misc/kibana-parking.json```); namely, we show assets that belongs to [SmartSantander](http://www.smartsantander.eu/).

| ![Data Management ](docs/kibana-example-1.png) |
|:--:|
| Parking estimator Data Management dashboard (Kibana) - current status |
----------
| ![Data Managemen](docs/kibana-example-2.png) |
----------
| Parking estimator Data Management dashboard (Kibana) - models/predictions stats |

# Support

The support for this service can be requested via [issue tracker](https://gitlab.com/synchronicity-iot/parking-estimator/issues)

# About

This service has been developed in the context of the [SynchroniCity project](https://synchronicity-iot.eu/) that aims to develop a common services and data marketplaces for IoT and Smart Cities.

# License

Different types of license are handled among the different modules carried out in the scope of this project. The reader can find more details within each of them (and their respective license files therein). As a summary, these are the options chosen:

- **IoT Data Manager**: AGPLv3
- **Service Manager**: AGPLv3
- **Training Engine**: AGPLv3
- **Inference Engine**: AGPLv3
- **Orion CB**: AGPLv3
- **Mongo DB**: Server Side Public License
- **ElasticSearch**: Apache 2.0
- **Kibana**: Apache 2.0

